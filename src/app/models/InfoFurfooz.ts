export class Info {
    Email: string;
    Telephone: string;
    ConservatorName: string;
    IsDeleted: boolean;
}