export class events {
    Id: number;
    Name_fr: string;
    Name_nl?: any;
    Name_en?: any;
    Image?: any;
    Description_fr?: any;
    Description_en?: any;
    Description_nl?: any;
    Latitude?: number;
    Longitude?: number;
    StartDate?: any;
    EndDate?: any;
    Interval?: any;
    IsDeleted: boolean;
    Category_id?: any;
    IsSeen: boolean;
    AlreadyShown: boolean;
    Distance?: number;
}