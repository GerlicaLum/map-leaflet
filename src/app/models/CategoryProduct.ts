export class CategoryProduct {
    id: number;
    name_fr: string;
    name_nl?: string;
    name_en?: string;
}