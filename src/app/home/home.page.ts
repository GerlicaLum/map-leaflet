import { Component, AfterContentInit, ChangeDetectorRef } from '@angular/core';
import{Map, tileLayer, marker, polyline, icon, latLng, latLngBounds, PanTo} from "leaflet";
import L from "leaflet";
import 'node_modules/leaflet-gpx';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { events } from '../models/events';
import { Vibration } from '@ionic-native/vibration/ngx';
import {LocalNotifications} from "@ionic-native/local-notifications/ngx";
import { AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';
import { EventsService } from '../services/events.service';
import { category } from '../models/Category';
import { CategoryService } from '../services/category.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterContentInit {
  
  
  CategoryPoiList: category[];
  map: Map; 
  marker: any;
  latLong = []; 
  position: any;
  compt = 0;
  southWest;
  northEast;
  Redmarkers = icon({
    iconUrl: 'assets/icon/marker-icon-2x-red.png',
    shadowUrl: 'assets/icon/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41],
  }) ;

  Markers: Object;

  slideOpts = {
    slidesPerView: 3,
    direction: 'horizontal',
    slideToClickedSlide: true,
    mousewheel: true,
    freeMode: true,
    freeModeMomentumBounce: false,
    freeModeMomentumRatio: 0.5,
    freeModeMomentumVelocityRatio: 0.5,
    centeredSlides: true,
    spaceBetween: 5
}
  

  myLocation;
  POI;
  // bounds = latLngBounds(this.latLong);
  eventList: events[];

  constructor(
    private geolocation: Geolocation,
    private detectChangeRef: ChangeDetectorRef,
    private localNotifications: LocalNotifications,
    private alertCtrl: AlertController,
    private vibration: Vibration,
    public modalController: ModalController,
    private eventService: EventsService,
    private catService: CategoryService,
    

    
  ) {}
  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        'EventList' : this.eventList.filter(x=> x.Distance > 0),
        swipeToClose: true
      }
    });
    return await modal.present();
  }

  showMap(){
    // this.map = new Map("myMap").setView([50.2132,4.9540],15);
    // tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    // attribution: '&copy; <a href="https://www.openstreetmap.org/copyright%22%3EOpenStreetMap</a> contributors',
    // minZoom: 15,
    // maxZoom: 18,
    // id: 'mapbox/streets-v11',
    // tileSize: 512,
    // zoomOffset: -1,
    // accessToken: 'your.mapbox.access.token'
    // }).addTo(this.map);

    let mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWF0aGlldWdpbGxhcmQiLCJhIjoiY2tmY2lkZHhwMTBuczMybzhseW42d3R0NSJ9.Xw8nn6zfWoCZtuK15d0V3w';
    

    let OutDoors = L.tileLayer(mbUrl, {id : "mapbox/outdoors-v11",
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    let Satellite = L.tileLayer(mbUrl, { id : "mapbox/satellite-v9",
      detectRetina: true,
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });     
    let baseLayers = {
      'Outdoor': OutDoors,
      'Satellite': Satellite
    }   
    this.map = L.map('myMap', {
      // center : [50.2132,4.9540],
      zoom : 15,
      layers : [ OutDoors],
      minZoom : 15,
      maxZoom : 18
    })


    // let southWest = L.latLng([50.2075, 4.9372]);
    // let northEast = L.latLng([50.2188, 4.9681]);
    this.southWest = L.latLng([50.3098479, 5.1034572]);
    this.northEast = L.latLng([50.310899, 5.1076968]);
   let bounds = L.latLngBounds(this.southWest, this.northEast);
   this.map.setMaxBounds(bounds);
   L.control.layers(baseLayers).addTo(this.map);
   
   this.catService.context.subscribe(data => {this.CategoryPoiList = data

    
    this.eventService.context.subscribe(data => {this.eventList = data
      console.log(this.CategoryPoiList);

      for(let cat of this.CategoryPoiList)
      {
        this.Markers[cat.Id] = [];
        for(let p of this.eventList.filter(x=> x.Category_id == cat.Id))
        {
          if(p.Latitude != null)
          {
            let markers : marker= marker([p.Latitude, p.Longitude], {icon: this.Redmarkers});
          this.myLocation =  { lat: this.position.coords.latitude, lng: this.position.coords.longitude };
          this.POI = { lat: p.Latitude, lng: p.Longitude };
           p.Distance = Math.round((this.getDistanceBetweenPoints(this.myLocation, this.POI, 'km')*1000));
           markers.addTo(this.map).bindPopup(`${p.Name_fr} <p> Distance: ${p.Distance}</p> `);
           console.log(this.southWest, this.northEast);
           // let _marker = marker([p.Latitude,p.Longitude], {icon: this.Redmarkers});
            // _marker.addTo(this.map).bindPopup(p.Name_fr);
            
            this.Markers[cat.Id].push(markers);
          }
        }
      }
    })
  })
   


  //  var gpx = 'assets/Gpx/TechnobelGeveko.gpx' // URL to your GPX file or the GPX itself

   
   
  //   new L.GPX(gpx, {async: true}).on('loaded', function(e) {
  //   this.map.fitBounds(e.target.getBounds());  
  //   }).addTo(this.map);

   
  }
  


  ngAfterContentInit() {

    this.Markers = [];
    // this.eventService.context.subscribe(x => this.eventList = x);
    // this.eventList=[
    //   {Name_fr: "Gibier", latitude: 50.311490, longitude: 5.106436, Interval: 0.02, IsSeen: false, AlreadyShown:false},
    //   {Name_fr: "Paulus", latitude:50.311013 , longitude: 5.106942, Interval: 0.02, IsSeen: false,AlreadyShown:false},
    //   {Name_fr: "Magasin de sport Chelou", latitude: 50.309997, longitude: 5.107739, Interval: 0.02, IsSeen: false, AlreadyShown:false},
    //   {Name_fr: "Geveko", latitude: 50.310989, longitude:  5.110055, Interval: 0.02, IsSeen: false, AlreadyShown:false}
    // ]
    setTimeout(() => { this.map.invalidateSize(true); }, 500);
    this.showMap();
    this.getPositions();

    
    
  }


  ionViewDidEnter() {
    //this.showMap();

  }
  getPositions() {

    this.geolocation.watchPosition({
      enableHighAccuracy : true,
      timeout: 500,
      
    }).subscribe(position => {
      
      if(position != null) {
        this.detectChangeRef.detectChanges();
        this.compt++;
        position = (position as Geoposition);
        for(let m of this.eventList || []){
          if (m.Latitude != null){
          // let markers : marker= marker([m.Latitude, m.Longitude], {icon: this.Redmarkers});
         // this.myLocation =  { lat: position.coords.latitude, lng: position.coords.longitude };
          // this.POI = { lat: m.Latitude, lng: m.Longitude };
        //  m.Distance = Math.round((this.getDistanceBetweenPoints(this.myLocation, this.POI, 'km')*1000));
        // markers.addTo(this.map).bindPopup(`${m.Name_fr} <p> Distance: ${m.Distance}</p> `);
          this.map.panTo([position.coords.latitude, position.coords.longitude]);

          let dist = this.getDistanceBetweenPoints(this.myLocation, this.POI, 'km');
          if(dist < m.Interval && m.AlreadyShown == false)
          {
            this.localNotifications.schedule({
              id: 1,
              text: `${m.Name_fr} est à ${m.Distance} m de votre position`,
              
            });
          }
          if(m.AlreadyShown==false)
          {

          if(dist < m.Interval && m.IsSeen == false)
          {
            this.vibration.vibrate(1000);
            let alert = this.alertCtrl.create({
              message: 
              `Voulez vous voir ${m.Name_fr} ?` ,
              buttons: [
                {
                  text: 'Non',
                  role: 'cancel',
                  handler: () => {
                    this.vibration.vibrate(0);
                    this.localNotifications.clear(1);
                  }
                },
                {
                  text: 'Oui',
                  handler: () => {
                    m.IsSeen = true;
                     this.localNotifications.clear(1);
                    
                    
                  }
                }
              ]
            });
            alert.then(a => {a.present()});
          }
          m.AlreadyShown = true;
          
          
        }
      }
    }     
        // console.log(position.coords);
        this.position = position;
        this.latLong = [ 
          position.coords.latitude,
          position.coords.longitude
        ];
        
        this.showMarker(this.latLong)
      }
      
      
    }, (e) => this.position = "cannot find position")
  }
  
  showMarker(latLong){
    if(this.marker == null) {
      this.marker = marker(this.latLong);
      this.marker.addTo(this.map)  
    }
    else{
      this.marker.setLatLng(this.latLong);
      
    }
    
  }
  getDistanceBetweenPoints(start, end, units){

    let earthRadius = {
        miles: 3958.8,
        km: 6371
    };

    let R = earthRadius[units || 'km'];
    let lat1 = start.lat;
    let lon1 = start.lng;
    let lat2 = end.lat;
    let lon2 = end.lng;

    let dLat = this.toRad((lat2 - lat1));
    let dLon = this.toRad((lon2 - lon1));
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
    Math.sin(dLon / 2) *
    Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return d;

  }

  toRad(x){
    return x * Math.PI / 180;
  }

  
  
  RemoveAllMarkers(id : number) {
    for(let key in this.Markers)
    {
      for(let marker of this.Markers[key])
      {
        this.map.removeLayer(marker);
      }
    }

    for(let m of this.Markers[id])
    {
      this.map.addLayer(m);
    }
  }
  SeeAllMarkers() {
    for(let key in this.Markers)
    {
      for(let marker of this.Markers[key])
      {
        this.map.removeLayer(marker);
      }
    }

    for(let key in this.Markers)
    {
      for(let marker of this.Markers[key])
      {
        this.map.addLayer(marker);
      }
    }
  }
  
}
  
  
  

