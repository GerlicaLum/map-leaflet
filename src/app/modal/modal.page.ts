import { Component, OnInit, Input } from '@angular/core';
import { events } from '../models/events';
import { ModalController } from '@ionic/angular'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})


export class ModalPage implements OnInit {

  @Input() EventList : events[]

  constructor(private modalController : ModalController) { }

  ngOnInit() {
  }
  
  Close() {
    this.modalController.dismiss();
  }

}
