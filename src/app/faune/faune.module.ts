import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FaunePageRoutingModule } from './faune-routing.module';

import { FaunePage } from './faune.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FaunePageRoutingModule,
    
    TranslateModule.forChild()
  ],
  declarations: [FaunePage]
})
export class FaunePageModule {}
