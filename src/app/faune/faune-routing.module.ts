import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FaunePage } from './faune.page';

const routes: Routes = [
  {
    path: '',
    component: FaunePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaunePageRoutingModule {}
