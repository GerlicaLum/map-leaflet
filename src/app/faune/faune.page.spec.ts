import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FaunePage } from './faune.page';

describe('FaunePage', () => {
  let component: FaunePage;
  let fixture: ComponentFixture<FaunePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaunePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FaunePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
