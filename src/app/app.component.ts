import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { environment } from 'src/environments/environment';
import {TranslateService} from '@ngx-translate/core';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public translate: TranslateService,
    private backgroundMode: BackgroundMode

  ) {
    this.initializeApp();

    translate.addLangs(['en', 'fr', 'nl']);
    translate.setDefaultLang(environment.selectedLanguage);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.backgroundMode.enable();

    });
  }
  setLangue(langue: string) {
    this.translate.use(langue)
  }
  
}

