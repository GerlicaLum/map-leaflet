import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 
  {
    path: '',
    redirectTo: 'accueil',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./modal/modal.module').then( m => m.ModalPageModule)
  },
  {
    path: 'accueil',
    loadChildren: () => import('./accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: 'flore',
    loadChildren: () => import('./flore/flore.module').then( m => m.FlorePageModule)
  },
  {
    path: 'faune',
    loadChildren: () => import('./faune/faune.module').then( m => m.FaunePageModule)
  },
  {
    path: 'buvette',
    loadChildren: () => import('./buvette/buvette.module').then( m => m.BuvettePageModule)
  },
  {
    path: 'horaires',
    loadChildren: () => import('./horaires/horaires.module').then( m => m.HorairesPageModule)
  },
  {
    path: 'infos',
    loadChildren: () => import('./infos/infos.module').then( m => m.InfosPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'sites',
    loadChildren: () => import('./sites/sites.module').then( m => m.SitesPageModule)
  },
  {
    path: 'genericpoi',
    loadChildren: () => import('./genericpoi/genericpoi.module').then( m => m.GenericpoiPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
