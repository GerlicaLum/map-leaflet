import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HorairesPageRoutingModule } from './horaires-routing.module';

import { HorairesPage } from './horaires.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HorairesPageRoutingModule,
    
    TranslateModule.forChild()
  ],
  declarations: [HorairesPage]
})
export class HorairesPageModule {}
