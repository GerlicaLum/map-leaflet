import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HorairesPage } from './horaires.page';

describe('HorairesPage', () => {
  let component: HorairesPage;
  let fixture: ComponentFixture<HorairesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorairesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HorairesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
