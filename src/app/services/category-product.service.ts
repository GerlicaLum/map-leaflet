import { Injectable } from '@angular/core';
import { CategoryProduct } from '../models/CategoryProduct';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryProductService {

  refresh(){
    this.http.get<CategoryProduct>(this.URL).subscribe(data=> 
     {this.context.next(data)});}

     context: BehaviorSubject<CategoryProduct>;

   private URL: string = 'https://10.10.2.53/ApiFurfooz/api/' + "categoryproduct"

  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<CategoryProduct>(null);
    this.refresh();
   } 
}
