import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { category } from '../models/Category';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
context: BehaviorSubject<category[]>
refresh(){
  this.http.get<category[]>(this.URL).subscribe(data=> 
   {this.context.next(data)});}


private URL: string = 'https://furfooz.somee.com/api/category'

  constructor(private http: HttpClient)  {
    this.context = new BehaviorSubject<category[]>([]);
    this.refresh();
   }
}
