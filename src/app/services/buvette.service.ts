import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Buvette } from '../models/Buvette';

@Injectable({
  providedIn: 'root'
})
export class BuvetteService {

   refresh(){
     this.http.get<Buvette[]>(this.URL).subscribe(data=> 
      {this.context.next(data)});
   }

  context: BehaviorSubject<Buvette[]>;
   private URL: string = 'https://furfooz.somee.com/api/buvette' 
  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<Buvette[]>(null);
    this.refresh();
   }

}
