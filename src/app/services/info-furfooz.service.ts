import { Injectable } from '@angular/core';
import { Info } from '../models/InfoFurfooz';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InfoFurfoozService {

  context: BehaviorSubject<Info>
refresh(){
  this.http.get<Info>(this.URL).subscribe(data=> 
   {this.context.next(data)});}


private URL: string = 'https://furfooz.somee.com/api/infofurfooz' 

  constructor(private http: HttpClient)  {
    this.context = new BehaviorSubject<Info>(null);
    this.refresh();
   }
}
