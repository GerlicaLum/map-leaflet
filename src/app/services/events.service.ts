import { Injectable } from '@angular/core';
import { events } from '../models/events';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  
 

  context: BehaviorSubject<events[]>


  
  refresh() {
    this.http.get<events[]>(this.URL).pipe(map((x) => {
      for(let item of x)
      {
        item.IsSeen = false;
        item.AlreadyShown = false;
        
      }
      return x;
    })).subscribe(data => this.context.next(data));
  }
  
  
  private URL: string = 'https://furfooz.somee.com/api/pointinteret'
  
  
    constructor(private http: HttpClient)  {
      this.context = new BehaviorSubject<events[]>(null);
      this.refresh();
     }
     
}
