import { Injectable } from '@angular/core';
import { Tarif } from '../models/Tarif';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TarifService {

  context: BehaviorSubject<Tarif>
refresh(){
  this.http.get<Tarif>(this.URL).subscribe(data=> 
   {this.context.next(data)});}


private URL: string = 'https://furfooz.somee.com/api/Tarif' 

  constructor(private http: HttpClient)  {
    this.context = new BehaviorSubject<Tarif>(null);
    this.refresh();
   }
}
