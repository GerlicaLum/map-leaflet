import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuvettePage } from './buvette.page';

const routes: Routes = [
  {
    path: '',
    component: BuvettePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuvettePageRoutingModule {}
