import { Component, OnInit } from '@angular/core';
import { Buvette } from '../models/Buvette';
import { BuvetteService } from '../services/buvette.service';


@Component({
  selector: 'app-buvette',
  templateUrl: './buvette.page.html',
  styleUrls: ['./buvette.page.scss'],
})
export class BuvettePage implements OnInit {

  buvetteList : Buvette[];

  constructor(private BuvetteService : BuvetteService) { 
    this.buvetteList = [];
  }

  ngOnInit() {
    
    this.BuvetteService.context.subscribe(data => {
      
      this.buvetteList = data?.filter(x => { 
        console.log(x);
        return x
       
      });
    })
  }
}