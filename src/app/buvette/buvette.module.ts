import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuvettePageRoutingModule } from './buvette-routing.module';

import { BuvettePage } from './buvette.page';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BuvettePageRoutingModule,
   
    TranslateModule.forChild()
  ],
  declarations: [BuvettePage]
})
export class BuvettePageModule {}
