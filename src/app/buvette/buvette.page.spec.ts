import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuvettePage } from './buvette.page';

describe('BuvettePage', () => {
  let component: BuvettePage;
  let fixture: ComponentFixture<BuvettePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuvettePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BuvettePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
