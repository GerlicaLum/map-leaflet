import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

  constructor() { }
  image:any;

  slideOpts = {
    initialSlide: 0,
    speed: 500,
    slidesPerView: 1,
    autoplay: true,
    loop: true
  }

  ngOnInit(){
    this.image ="../../assets/img/Ancien_10.png";
    }
    
 
    }
    