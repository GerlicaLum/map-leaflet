import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { events } from '../models/events';
import { EventsService } from '../services/events.service';
import {TranslateService, TranslateLoader} from '@ngx-translate/core';

@Component({
  selector: 'app-genericpoi',
  templateUrl: './genericpoi.page.html',
  styleUrls: ['./genericpoi.page.scss'],
})
export class GenericpoiPage implements OnInit {

@Input() IdPOI:number;

poi : events[];

  constructor(private modalcontroler : ModalController,
    private eventsService: EventsService,
    private translate: TranslateLoader,
    private translatemodule: TranslateService) { }

  ngOnInit() {
    this.eventsService.context.subscribe(data => {
      this.poi = data.filter(x => {
        return x.Id == this.IdPOI
      });
    })
  }

  close() {
    this.modalcontroler.dismiss();
  }

}
