import { Component, OnInit, Input } from '@angular/core';
import { events } from '../models/events';
import { EventsService } from '../services/events.service';
import { Event } from '@angular/router';
import { GenericpoiPage } from '../genericpoi/genericpoi.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.page.html',
  styleUrls: ['./sites.page.scss'],
})
export class SitesPage implements OnInit {
  IdCat : number = 1;
  PoiList : events[];

  constructor(private eventService : EventsService,
    private modalcontroler : ModalController) {
    this.PoiList = []
   }

  ngOnInit() {
    this.eventService.context.subscribe(data => {
      
      this.PoiList = data?.filter(x => {
        return x.Category_id == this.IdCat
      });
    })
  }

  async poiModal(ID: number){
    const modal = await this.modalcontroler.create({
      component: GenericpoiPage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {'IdPOI' : ID}
    });
    return await modal.present();
  }

}
