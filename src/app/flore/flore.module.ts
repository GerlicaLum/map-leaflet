import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlorePageRoutingModule } from './flore-routing.module';

import { FlorePage } from './flore.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlorePageRoutingModule,
    
    TranslateModule.forChild()
  ],
  declarations: [FlorePage]
})
export class FlorePageModule {}
