import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlorePage } from './flore.page';

describe('FlorePage', () => {
  let component: FlorePage;
  let fixture: ComponentFixture<FlorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FlorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
