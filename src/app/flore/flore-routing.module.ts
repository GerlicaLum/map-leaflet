import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlorePage } from './flore.page';

const routes: Routes = [
  {
    path: '',
    component: FlorePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlorePageRoutingModule {}
