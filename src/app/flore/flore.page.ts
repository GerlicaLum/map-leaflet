import { Component, OnInit, Input } from '@angular/core';
import { events } from '../models/events';
import { EventsService } from '../services/events.service';

@Component({
  selector: 'app-flore',
  templateUrl: './flore.page.html',
  styleUrls: ['./flore.page.scss'],
})
export class FlorePage implements OnInit {

  IdCat: number = 2;
  PoiList : events[];

  constructor(private eventService : EventsService) { 
    this.PoiList = [];
  }

  ngOnInit() {
    this.eventService.context.subscribe(data => {
      
      
      this.PoiList = data?.filter(x => { 
        console.log(x);
        return x.Category_id == this.IdCat
       
      });
    })
  }

}
